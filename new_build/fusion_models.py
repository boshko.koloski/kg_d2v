#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 12:31:28 2020

@author: bosko
"""

import torch
import torch.nn as nn
import numpy as np
from torch.utils.data import Dataset, DataLoader, random_split

def test_val(dataset, test_split, batch_size):
    torch.manual_seed(1903)

    dataset_size = len(dataset)
    test_size = int(test_split * dataset_size)
    train_size = dataset_size - test_size      

    train_dataset, test_dataset = random_split(dataset, [train_size, test_size])


    train_loader = DataLoader(train_dataset.dataset, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_dataset.dataset, batch_size=batch_size, shuffle=True)

    return train_loader, test_loader 


class ColumnarDataset(Dataset):
   def __init__(self, df, y):
       self.x = df
       self.y = y
        
   def __len__(self): 
       return len(self.y) - 1
    
   def __getitem__(self, idx):
       row = self.x.iloc[idx, :].to_list()      
       return np.array(row), self.y[idx] #[self.x[idx], self.conts[idx], self.y[idx]]


class Net(nn.Module):    
  def __init__(self, n_features, embedding_dim = 256):
    super(Net, self).__init__()
    self.a1 = nn.Linear(n_features, embedding_dim)
    self.a2 = nn.Linear(embedding_dim, 2)
   
  def forward(self, x):
    x = torch.relu(self.a1(x))
    return torch.sigmoid(self.a2(x))

class Net2(nn.Module):    
  def __init__(self, n_features, embedding_dim = 256, embedding_dim2 = 64):
    super(Net2, self).__init__()
    self.a1 = nn.Linear(n_features, embedding_dim)
    self.a2 = nn.Linear(embedding_dim, embedding_dim2)
    self.a3 = nn.Linear(embedding_dim2, 2)
   
  def forward(self, x):
    x = torch.tanh(self.a1(x))
    x = torch.tanh(self.a2(x))
    return torch.sigmoid(self.a3(x))

