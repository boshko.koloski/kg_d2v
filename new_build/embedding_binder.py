#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Thu Oct 24 11:06:38 2020

@author: daskalot

"""
#import experiments 
import fusion_models
from fusion_models import Net, ColumnarDataset
from tqdm import tqdm
import numpy as np
import pandas as pd
import torch 
import torch.nn as nn
import torch.optim as optim
from sklearn import preprocessing
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns

#UTILITY
import os 
import shutil
import pickle

def embedding_bilinear(embedding1, embedding2, ratio1to2 = 0.5):
    ratio2to1 =  1 - ratio1to2
    assert embedding1.shape == embedding2.shape
    new_embedding = ratio2to1 * embedding1 + ratio1to2 * embedding2 
    return new_embedding

def embedding_trilinear(embedding1, embedding2, embedding3, ratio_first = 0.5, ratio_second = 0.5):
    embedding_first_second = embedding_bilinear(embedding1, embedding2, ratio_first = 0.5)
    embedding_second_third = embedding_bilinear(embedding_first_second, embedding3, ratio_second = 0.5)
    return embedding_second_third 

def embedding_merger(embedding1, embedding2 = None, embedding3 = None, strategy = "normalize"):
    if strategy == "normalize" and embedding1 and not embedding2:
        return preprocessing.scale(embedding1)
    if strategy == "bilinear":     
        return embedding_bilinear(embedding1, embedding2)
    if strategy == "trilinear":
        return embedding_trilinear(embedding1, embedding2, embedding3)




def export_img(df, y, name="namex"):
    print(len(df), len(y))
    sklearn_pca = PCA(n_components=2)
    embedding = sklearn_pca.fit_transform(df)
    sns_plot = sns.scatterplot(x=embedding[:,0], y=embedding[:,1], s = 150, hue=y)
    sns_plot.figure.savefig(name)
    plt.clf()

    
def prepare_paths(model_name, visualize, export):
        
    kg_data = {}
    embs = {}
    imgs_path = None
    exports_path = None    
    
    if visualize:
        imgs_path = os.path.join(model_name, "imgs")
        if os.path.exists(imgs_path):
            shutil.rmtree(imgs_path)
        os.makedirs(imgs_path, exist_ok = True)     
    
    if export:
        exports_path = os.path.join(model_name, "transform_matrix")
        if os.path.exists(exports_path):
          shutil.rmtree(exports_path)
        os.makedirs(exports_path, exist_ok = True)  
    
    for folder in os.listdir(model_name):
        src_path = os.path.join(model_name,folder)
        if not os.path.isdir(src_path):
            continue
        for file in os.listdir(src_path):
            file_name = file[:-4]
            if folder == "imgs" or folder == "transform_matrix":
                continue
            if folder == "kg_embeddings":
                kg_data[file_name] = os.path.join(src_path, file)
            else:
                embs[file_name] = os.path.join(src_path, file)            
            
    paths = { "embs" : embs,
              "kg_data" : kg_data,
              "imgs_path" : imgs_path,
              "exports_path" : exports_path}
    return paths
    #return embs, kg_data, imgs_path, exports_path

def prepare_data(paths, kg, emb, y, visualize):
    kg_path = paths['kg_data'][kg]
    emb_path = paths['embs'][emb]
    df1 = pd.read_csv(kg_path, header=None)
    df2 = pd.read_csv(emb_path, header=None)
    
    if visualize:
        tmp_path = os.path.join(paths['imgs_path'],emb)
        os.makedirs(tmp_path, exist_ok = True)
        export_img(df1, y, os.path.join(tmp_path,kg))
        export_img(df2, y, os.path.join(tmp_path,emb))  
    
    concated = pd.concat([df1, df2], axis=1)
   
    return concated


def evaluate_env(model, test_loader, device=torch.device("cuda"), scoring_function = None):
    orgs = []
    preds = []
    model.eval()
    with torch.no_grad():
        for i, data in enumerate(test_loader, 0):
            inputs, labels = data
            inputs, labels =  inputs.to(device), labels.to(device)
            outputs = model(inputs.float())
            logits = outputs.cpu().numpy()
            labels = labels.cpu().numpy()
            guessed = np.argmax(logits,axis=1)
            orgs = orgs + labels.tolist()
            preds = preds + guessed.tolist()

    if scoring_function:
        return scoring_function(orgs, preds)
    else:
        return orgs, preds

def train_env(model_name, y, visualize = True, export = True, hyper_params = {'params' : {'batch_size': 30, 'shuffle': False}, 'max_epochs' : 500, 'scoring_function' : f1_score}):
    #Get the models parameters
    max_epochs = hyper_params['max_epochs']
    params = hyper_params['params']
    scoring_function = hyper_params["scoring_function"] if "scoring_function" in hyper_params else f1_score
    #Prepare paths for models 
    paths = prepare_paths(model_name, visualize, export)    
         
    #Set the device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    #Scores
    scores = dict()
    scores["epochs"] = list(range(max_epochs))
    #scoring_function = hyper_params["scoring_function"] if not "scoring_function" in hyper_params else None

    for kg in tqdm(paths['kg_data'], total=len(paths['kg_data'])):
        for emb in tqdm(paths['embs'],total=len(paths['embs'])):            
            for dims in [128,256,512,1024,2048]:
                tmp_combo_name = "_".join([str(x) for x in [emb, dims]])  
                print("\n",tmp_combo_name,"\n")                
                
                #Prepare the datasets                
                concated = prepare_data(paths, kg, emb, y, visualize)
                dataset = ColumnarDataset(concated, y)     
                feature_dims = concated.shape[1]
                del concated                 

                #Split the data into train and test
                train_loader, test_loader = fusion_models.test_val(dataset, 0.25, params['batch_size'])

                #Init the network
                net = Net(feature_dims, dims)
                net = net.to(device)
                criterion = nn.CrossEntropyLoss()
                optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)                                          
                                                               
                #Scores:
                scores[tmp_combo_name] = []
                
                #Model to export
                best_score = -1.0
                out_model = None
                
                for epoch in tqdm(range(max_epochs), total = max_epochs):  
                    running_loss = 0.0
                    
                    for i, data in enumerate(train_loader, 0):

                        inputs, labels = data
                        inputs, labels =  inputs.to(device), labels.to(device)
                        optimizer.zero_grad()
                
                        outputs = net(inputs.float())
                        loss = criterion(outputs, labels)
                        loss.backward()
                        optimizer.step()
                
                        running_loss += loss.item()
                        if epoch % (max_epochs/2) == 0:
                            print('[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss / 10))
                            running_loss = 0.0                        
                                      
                    orgs, preds = evaluate_env(net, test_loader, device) 
                    score = scoring_function(orgs, preds)
                    scores[tmp_combo_name].append(score)
                    
                    if score > best_score:
                        best_score = score
                        out_model = net
                    if epoch % 10 == 0:
                        print("VALIDATION SCORE: ", score)

                #Generate the outputs
                weights = out_model.a1.weight
                nx = weights.cpu().detach().numpy()
                bias = out_model.a1.bias.cpu().detach().numpy().reshape(-1,1)
                
                concated = prepare_data(paths, kg, emb, y, visualize=False)
                vals = nx.dot(concated.values.T) + bias
                del concated
                
                if visualize:
                    new_space = os.path.join(paths['imgs_path'],emb + kg)
                    os.makedirs(new_space, exist_ok = True)
                    export_img(vals.T, y, new_space + "/" + str(dims))
                    
                if export:
                    df_out = pd.DataFrame(vals)
                    new_space = os.path.join(paths['exports_path'], emb + kg)
                    os.makedirs(new_space, exist_ok = True)
                    df_out.to_csv(new_space + "/"+ str(dims) + ".csv",header=None,index=False)
                
        with open(os.path.join(model_name,kg+"_scores.pkl"),"wb") as f:
            pickle.dump(scores, f)


def get_ys(dataset_path):
    import csv
    for dataset in dataset_path:
       states = ["train", "dev", "test"]
       cv_data = {"text_a" : [], "label" : []}
       dataset = dataset_path[dataset]
       for state in states:
           path = "data/"+dataset+"/"+state+".tsv"
           temp = pd.read_csv(path, sep="\t", quoting = csv.QUOTE_NONE, escapechar = ' ')
           cv_data['label'] = cv_data['label'] + temp['label'].tolist()
       return cv_data['label']
   
    
def learn_bindings():
    problems = { "fake_news_spreaders" : "pan-2020-fake-news"}      
    hyper_params = { "fake_news_spreaders" : {'params' : {'batch_size': 30, 'shuffle': False}, 'max_epochs' : 500, 'scoring_function' : accuracy_score}}
    for problem in problems:
        y = get_ys({problem : problems[problem]}) #})
        train_env(problems[problem], y, visualize=True, export=True, hyper_params = hyper_params[problem])

#learn_bindings()