#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 10:53:33 2020

@author: bosko
"""

import pickle
import os
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
import pandas as pd
import seaborn as sns

def prepare(folder_name = "dummy"):
    with open(os.path.join(folder_name, "present_entities.pkl"),"rb") as f:
        entities_doc = pickle.load(f)
    data_over = []
    for v in ["train","test","dev"]:
        dataframe = pd.read_csv(os.path.join("data",folder_name))     
        data_over = data_over + dataframe["text_a"]
    df = pd.DataFrame(data=data_over)
    del data_over
    return df, entities_doc

def analyse_kg_entries(model_name):
    outs = []
    zeros = 0
    cnts = []
    with open(os.path.join(model_name,"present_entities.pkl"),"rb") as f:
        data = pickle.load(f)
    print(type(data))
    print(len(data))    
    for x in tqdm(data):
        outs = outs + x
        if len(x) == 0: zeros = zeros + 1
        cnts.append(len(x))    
    print(len(data))
    cnts = np.array(cnts)
    print("MEAN", np.mean(cnts))
    print("STD", np.std(cnts))
    print("MAX", np.max(cnts))
    print(data[np.argmax(cnts)])
    print("MIN", np.min(cnts))
    print(data[np.argmin(cnts)])
    print("NO ENTITEIS", zeros)
    
    counts = Counter(outs)    
    di = counts.most_common(50)
    df = pd.DataFrame(di, columns=["tag", "value"])
    df = df.sort_values('value', ascending=False)
    plot = df.plot(xticks=df.index, kind='bar', rot=45)
    plot.set_xticklabels(df.tag)   
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()

    plot.figure.savefig(os.path.join(model_name, "imgs", "entities_analysis.png"),dpi=300)
        
    
def analyse_learning(model_name):
    for kg in ["transe", "distmult", "complex"]:
        with open(os.path.join(model_name,kg+"_scores.pkl"),"rb") as f:
            scores = pickle.load(f)
        
        df = pd.DataFrame(scores)
        plot = sns.lineplot(x='epochs', y='value', hue='variable', data=pd.melt(df, ['epochs']))
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.show()
        plot.figure.savefig(os.path.join(model_name, "imgs",kg+"learning_analysis.png"),dpi=300)
            
def analyser(model_name):
    analyse_kg_entries(model_name)
    analyse_learning(model_name)
    
    
analyser("fake_news_spreaders")
