#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 11:02:30 2020

@author: bosko
"""

import os
import logging
from doc_embedders import Doc2VecTransformer
from doc_embedders import TfIdfTransformer
from doc_embedders import BERTTransformer
import numpy as np
try:
    from term2vec import Term2Vec
except:
    pass

logging.basicConfig(filename='execution.log',format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S',level=logging.DEBUG)

def prepare_data(data, dataname):
    logging.info("Prepearing dictionary from data for %s" % (dataname))        
    docs = dict(zip(range(len(data)),data))
    return docs

def vectorize_kg(docs, dataset_name, kg_models, top_n = 100):
    logging.info("Prepearing KG embeddings for dataset %s" % (dataset_name)) 
    
    #Organization
    path = os.path.join(dataset_name, "kg_embeddings")
    os.makedirs(path, exist_ok=True)
        
    for kg_model in kg_models:
        logging.info("Begin training %s embedding." % (kg_model))
        tmp_path = os.path.join(path,kg_model)
        t2v = Term2Vec(docs, dataset_name)
        t2v.fit(top_n, kg_model)        
        print(tmp_path)
        t2v._exportModel(tmp_path, "csv")
        logging.info("Completed training %s embedding." % (kg_model))
    logging.info("Prepearing KG embeddings for this file")

def vectorize_BERT(docs, dataset_name, bert_models):
    
    #Organization
    path = os.path.join(dataset_name, "bert")
    os.makedirs(path, exist_ok=True)
    
    #PREPARE DistilBERT
    for model in bert_models:
        logging.info("Prepearing BERT embeddings for dataset %s" % (dataset_name))    
        tmp_path = os.path.join(path, model)
        model = BERTTransformer(docs)
        model.fit()
        matrix = model.to_matrix()
        model._exportModel(tmp_path, "csv")
        import_matrix = model._importModel(tmp_path, "csv")    
        try: 
            assert (matrix == import_matrix).any()
            logging.info("DONE Prepearing BERT embeddings for dataset %s" % (dataset_name))    
        except Exception as e:
            print(e)
            logging.warning("FAILED Prepearing BERT embeddings for dataset %s" % (dataset_name))   

def vectorize_Doc2Vec(docs, dataset_name):
    # PREPARE DOC2VEC
    path = os.path.join(dataset_name, "doc2vec")
    os.makedirs(path, exist_ok=True)
    
    logging.info("Prepearing Doc2Vec embeddings for dataset %s" % (dataset_name))    
    model = Doc2VecTransformer(docs)
    model.fit()
    matrix = model.to_matrix()
    model._exportModel(os.path.join(path,"d2v"))
    import_matrix = model._importModel(os.path.join(path,"d2v"))    
    try: 
        assert (matrix == import_matrix).any()
        logging.info("DONE Prepearing Doc2Vec embeddings for dataset %s" % (dataset_name))    
    except Exception as e:
        print(e)
        logging.warning("FAILED Prepearing Doc2Vec embeddings for dataset %s" % (dataset_name))    
    
def vectorize_TFIDF(docs, dataset_name):
    logging.info("Prepearing TF-IDF embeddings for dataset %s" % (dataset_name))    
    path = os.path.join(dataset_name, "tfidf")
    os.makedirs(path, exist_ok=True)    
    model = TfIdfTransformer(docs,1,1)
    model.fit()
    matrix = model .to_matrix()
    model._exportModel(os.path.join(path,"tfidf"))
    import_matrix = model ._importModel(os.path.join(path,"tfidf"))    
    try: 
        assert (matrix.todense() == import_matrix.any())
        logging.info("DONE Prepearing TF-IDF embeddings for dataset %s" % (dataset_name))    
    except Exception as e:
        print(e)
        logging.warning("FAILED Prepearing TF-IDF embeddings for dataset %s" % (dataset_name))    
        
def vectorize(datasets, bert_models, kg_models, top_n = 10000):
    for name in datasets:
        #MAKE DATASET FOLDER
        os.makedirs(name, exist_ok = True)
        #Load the datasets        
        tmp_data = datasets[name]
        docs = prepare_data(tmp_data['text_a'], name)        
        #vectorize_TFIDF(docs, name)
        vectorize_Doc2Vec(docs, name)
        vectorize_BERT(docs, name, bert_models)
        vectorize_kg(docs, name, kg_models, top_n)
  
def test_env():
    train_text = ["Brexit (/ˈbrɛksɪt, ˈbrɛɡzɪt/;[1] a portmanteau of British and exit) is the withdrawal of the United Kingdom (UK) from the European Union (EU). Following a referendum held on 23 June 2016 in which 51.9 per cent of those voting supported leaving the EU, the Government invoked Article 50 of the Treaty on European Union, starting a two-year process which was due to conclude with the UK's exit on 29 March 2019 – a deadline which has since been extended to 31 October 2019.[2]","Withdrawal from the EU has been advocated by both left-wing and right-wing Eurosceptics, while pro-Europeanists, who also span the political spectrum, have advocated continued membership and maintaining the customs union and single market. The UK joined the European Communities (EC) in 1973 under the Conservative government of Edward Heath, with continued membership endorsed by a referendum in 1975. In the 1970s and 1980s, withdrawal from the EC was advocated mainly by the political left, with the Labour Party's 1983 election manifesto advocating full withdrawal. From the 1990s, opposition to further European integration came mainly from the right, and divisions within the Conservative Party led to rebellion over the Maastricht Treaty in 1992. The growth of the UK Independence Party (UKIP) in the early 2010s and the influence of the cross-party People's Pledge campaign have been described as influential in bringing about a referendum. The Conservative Prime Minister, David Cameron, pledged during the campaign for the 2015 general election to hold a new referendum—a promise which he fulfilled in 2016 following pressure from the Eurosceptic wing of his party. Cameron, who had campaigned to remain, resigned after the result and was succeeded by Theresa May, his former Home Secretary. She called a snap general election less than a year later but lost her overall majority. Her minority government is supported in key votes by the Democratic Unionist Party.","The broad consensus among economists is that Brexit will likely reduce the UK's real per capita income in the medium term and long term, and that the Brexit referendum itself damaged the economy.[a] Studies on effects since the referendum show a reduction in GDP, trade and investment, as well as household losses from increased inflation. Brexit is likely to reduce immigration from European Economic Area (EEA) countries to the UK, and poses challenges for UK higher education and academic research. As of May 2019, the size of the divorce bill—the UK's inheritance of existing EU trade agreements—and relations with Ireland and other EU member states remains uncertain. The precise impact on the UK depends on whether the process will be a hard or soft Brexit."]
    datasets = {"dummy" : {"text_a" : train_text, "label" : [1] * len(train_text)} }  
    kg_models = ["transe", "distmult", "complex"]
    bert_models = ["distilbert-base-nli-mean-tokens"]
    vectorize(datasets, bert_models, kg_models)  

#test_env()    
