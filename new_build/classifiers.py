#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 11:26:11 2020

@author: bosko
"""

import pandas as pd
import numpy as np
import experiments
import os
import shutil


def prepare_data():
    pass

def learn(data_path = {"fake_news_spreaders" : "pan-2020-fake-news"}, export = True):
    model_name = list(data_path.keys())[0]
    y = experiments.get_ys(data_path)
           
    if export:
        exports_path = os.path.join(model_name, "evaluations")
        if os.path.exists(exports_path):
              shutil.rmtree(exports_path)
        os.makedirs(exports_path, exist_ok = True)      
    
