#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Thu Oct 18 07:06:38 2020

@author: daskalot

"""

from doc_embedders import TfIdfTransformer as tfidf

from sklearn import preprocessing
import graphvite as gv
import numpy as np
from tqdm import tqdm
import pickle
import os

        
def find_words(docs, topn = 500):
    """
    Parameters
    ----------
    docs : List[str]
        List of texts to embedd.
    topn : int, optional
        Number of features needed to learn the embedding. The default is 50.

    Yields
    ------
    Tuple(Float, Float)
        Returns n-grams with their correspoinding TFIDF values sorted by their TFIDF values.
    """
    if not type(docs) == type(dict()):
        doc = {}
        for i,d in enumerate(docs):
            doc[i] = d
        docs = doc        
    tf = tfidf(docs)
    tf.fit()
    for d in docs:
        yield tf.doc2names(d, topn)
        
        
def make_embedding(term2vec):
    if len(term2vec) == 0:
        return np.array([-12]*512)
    avg = sum(list(term2vec.values())) / len(term2vec.values())  
    return avg

def embedd(texts, top_n = 500, model_name="transe", normalization = "equal", dataset_name = "name"):
    """  
    Parameters
    ----------
    texts: List[str]
        List of documents to be embedded.
    topn : int, optional
        Number of features needed to learn the embedding. The default is 50.
    model_name : str, optional
        Knowledge graph embedding model name as per defined by the graphvite library. The default is "transe".
    normalization: str, optional
        Normalization strategy: equal = every found embedding weighed equally
                                tfidf= every foun embedding is weighed by it's corresponding tfidf weight
        The default is "equal"
    Returns
    -------
    embeddings : List[Vector[floats]]
        List of embeddings constructed by the relations and entities found in the knowledge graph
    .  
    """
    
    embeddings = []
    present_words = []
    with open(model_name+".pkl", "rb") as fin:
        model = pickle.load(fin)
    alias2entity = gv.dataset.wikidata5m.alias2entity
    entity2id = model.graph.entity2id
    entity_embeddings = model.solver.entity_embeddings
    
    relation2id = model.graph.relation2id
    relation_embeddings = model.solver.relation_embeddings
    alias2relation = gv.dataset.wikidata5m.alias2relation
    
    path = os.path.join(dataset_name,"present_entities.pkl")
    if os.path.exists(path):
        present_words = pickle.load(open(path,'rb')) 
        for doc in present_words:
            term2vec = {}
            for term in doc:
                found = 1
                try:
                    if term in alias2entity :
                        if normalization == "equal":
                            term2vec[term] = entity_embeddings[entity2id[alias2entity[term]]]                    
                except:
                        if normalization == "equal":
                            term2vec[term] = relation_embeddings[relation2id[alias2relation[term]]]
            embeddings.append(make_embedding(term2vec))
    else:
        for doc_feats in find_words(texts, topn=top_n):
            not_found = 0
            term2vec = {}   
            for prob, term in doc_feats: #tqdm(doc_feats,total=len(doc_feats)):
                found = 1
                try:
                    if term in alias2entity :
                        if normalization == "equal":
                            term2vec[term] = entity_embeddings[entity2id[alias2entity[term]]]                    
                        if normalization == "tfidf":
                            term2vec[term] = prob * entity_embeddings[entity2id[alias2entity[term]]]
                except:
                    found = 0
                try:
                    if term in alias2relation :
                        if normalization == "equal":
                            term2vec[term] = relation_embeddings[relation2id[alias2relation[term]]]
                        if normalization == "tfidf":
                            term2vec[term] = prob * relation_embeddings[relation2id[alias2relation[term]]]
                except:
                    found = found | 0
                not_found = not_found + found
            #print("From the top %d values only %d found.  %d" % (top_n, top_n - not_found , len(term2vec.keys())))  
                      
            embeddings.append(make_embedding(term2vec))
            present_words.append(list(term2vec.keys()))
    if not os.path.exists(path):
        pickle.dump(present_words, open(path, "wb"))
    return embeddings
         
    
def test_env():
    train_text = {"text1":"Brexit (/ˈbrɛksɪt, ˈbrɛɡzɪt/;[1] a portmanteau of British and exit) is the withdrawal of the United Kingdom (UK) from the European Union (EU). Following a referendum held on 23 June 2016 in which 51.9 per cent of those voting supported leaving the EU, the Government invoked Article 50 of the Treaty on European Union, starting a two-year process which was due to conclude with the UK's exit on 29 March 2019 – a deadline which has since been extended to 31 October 2019.[2]","text2":"Withdrawal from the EU has been advocated by both left-wing and right-wing Eurosceptics, while pro-Europeanists, who also span the political spectrum, have advocated continued membership and maintaining the customs union and single market. The UK joined the European Communities (EC) in 1973 under the Conservative government of Edward Heath, with continued membership endorsed by a referendum in 1975. In the 1970s and 1980s, withdrawal from the EC was advocated mainly by the political left, with the Labour Party's 1983 election manifesto advocating full withdrawal. From the 1990s, opposition to further European integration came mainly from the right, and divisions within the Conservative Party led to rebellion over the Maastricht Treaty in 1992. The growth of the UK Independence Party (UKIP) in the early 2010s and the influence of the cross-party People's Pledge campaign have been described as influential in bringing about a referendum. The Conservative Prime Minister, David Cameron, pledged during the campaign for the 2015 general election to hold a new referendum—a promise which he fulfilled in 2016 following pressure from the Eurosceptic wing of his party. Cameron, who had campaigned to remain, resigned after the result and was succeeded by Theresa May, his former Home Secretary. She called a snap general election less than a year later but lost her overall majority. Her minority government is supported in key votes by the Democratic Unionist Party.","text3":"The broad consensus among economists is that Brexit will likely reduce the UK's real per capita income in the medium term and long term, and that the Brexit referendum itself damaged the economy.[a] Studies on effects since the referendum show a reduction in GDP, trade and investment, as well as household losses from increased inflation. Brexit is likely to reduce immigration from European Economic Area (EEA) countries to the UK, and poses challenges for UK higher education and academic research. As of May 2019, the size of the divorce bill—the UK's inheritance of existing EU trade agreements—and relations with Ireland and other EU member states remains uncertain. The precise impact on the UK depends on whether the process will be a hard or soft Brexit."}
    embedd(train_text)