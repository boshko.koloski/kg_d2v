# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 20:03:03 2020

@author: Bosec
"""
import xml.etree.ElementTree as ET
import numpy
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
import time
import csv
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from sklearn.decomposition import TruncatedSVD
import pickle
import umap

kg_map = {"./test_env/wikidata/complex_wikidata5m.pkl":"ComplEx",
          "./test_env/wikidata/rotate_wikidata5m.pkl":"RotatE",
          './test_env/wikidata/simple_wikidata5m.pkl':'SimplE',
          "./test_env/wikidata/distmult_wikidata5m.pkl":"DistMult",
          "./test_env/wikidata/transe_wikidata5m.pkl":"TransE",
          "./test_env/wikidata/quate_wikidata5m.pkl":"QuatE"
          }

def do(name, model, ys):
    reduced_matrix_form = model
    sns.set_style("whitegrid")
    reducer = umap.UMAP(random_state=42)
    embedding = reducer.fit_transform(reduced_matrix_form)    
    fig = sns.scatterplot(x=embedding[:,0], y=embedding[:,1], s = 150, hue=ys)
    #plt.show()
    fig.grid(False)

    fig.figure.savefig(kg_map[name] +'_e.svg', format='svg', dpi=300)    
    plt.clf()

Xs = []
ys = []
with open('eng.csv',encoding='utf-8', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file,delimiter=',')
    line_count = 0 
    for row in csv_reader:
              Xs.append(row[0])
              ys.append(row[1]) #print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')

     
models = pickle.load(open(os.path.join("fake_news_data2.pkl"),'rb'))
reducer = umap.UMAP(random_state=42)
embedding = reducer.fit_transform(Xs)    
fig = sns.scatterplot(x=embedding[:,0], y=embedding[:,1], s = 150, hue=ys)
fig.grid(False)

fig.figure.savefig('eng'+'_e.svg', format='svg', dpi=300)    
plt.clf()

#i = 0


#for x in models:
#    do(x,models[x],ys)
    