import pandas as pd
import os
import glob
import pickle
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords
import config
import nltk
import csv
import string
import config
from tqdm import tqdm
import graphvite as gv
import xml.etree.ElementTree as ET
import config
import numpy as np
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
import time
import csv
import config
from feature_construction import *
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import TruncatedSVD
import pickle

def remove_punctuation(text):

    """
    This method removes punctuation
    """

    table = text.maketrans({key: None for key in string.punctuation})
    text = text.translate(table)
    return text

def remove_stopwords(text):

    """
    This method removes stopwords
    """

    stops = set(stopwords.words("english")).union(set(stopwords.words('spanish')))
    text = text.split()
    text = [x.lower() for x in text if x.lower() not in stops]
    return " ".join(text)
def embed(t2i): 
    for kg in tqdm(glob.glob("./test_env/wikidata/*.pkl")):
        found = 0
        not_found = 0
        print("KG: "+kg)
        with open(kg, "rb") as fin:
            model = pickle.load(fin)
        entity2id = model.graph.entity2id
        entity_embeddings = model.solver.entity_embeddings
        alias2entity = gv.dataset.wikidata5m.alias2entity
        t2k[kg] = {}
        t2e = {}
        for i in t2i:
         txt = t2i[i]
         fnd = []
         cnt = 0
         for w in txt:
             try:
                fnd.append(entity_embeddings[entity2id[alias2entity[w]]])
                cnt = cnt + 1
             except Exception as e:
                 print(e)
         print(len(fnd))
         if cnt != 0:
             t2e[i] = sum(fnd)/cnt
             found = found + 1
         else:
             not_found = not_found + 1
        t2k[kg] = t2e
        print(f'FOUND : {found} NOT FOUND: {not_found}')
        pickle.dump(t2k,open(os.path.join(config.pickles,"t2k.pkl"),"wb"))
        
def train2(final_texts,emb,final_y,kg,output=False):
    print("KG: "+kg)
    best = 0
    d = None
    with open('fake_news_tfidf_emb.tsv', 'a+') as tsvfile:        
        writer = csv.writer(tsvfile, delimiter='\t')
        writer.writerow(['MODEL','KG USED','CV ACC','CV STD','TEST ACC'])
        for nrep in range(1):
                    n = np.array(emb)   
                    X_train, X_test, y_train, y_test = train_test_split(n, final_y, train_size=0.9, test_size=0.1 ,random_state = 0)
                    parameters = {"loss":["hinge","log"],"penalty":["elasticnet"],"alpha":[0.01,0.001,0.0001,0.0005],"l1_ratio":[0.05,0.25,0.3,0.6,0.8,0.95],"power_t":[0.5,0.1,0.9]}
                    svc = SGDClassifier()
                    clf1 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                    scores = cross_val_score(clf1, n, final_y, cv=5,scoring='accuracy')
                    acc_svm = scores.mean()
                    clf1.fit(X_train, y_train)   
                    predictions = clf1.predict(X_test) 
                    dim = 500
                    writer.writerow(['SGD_KG',kg,scores.mean(),scores.std(),accuracy_score(predictions,y_test)])
                    logging.info("SGD Training 5fCV Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
                    logging.info(str(accuracy_score(predictions,y_test)) + " training-eval configuration with best score (SGD,{})".format(dim))
                    parameters = {"C":[0.1,1,10,25,50,100,500],"penalty":["l2"]}
                    svc = LogisticRegression(max_iter = 100000)
                    clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                    scores = cross_val_score(clf2,  n, final_y, cv=5,scoring='accuracy') 
                    clf2.fit(X_train, y_train)
                    predictions = clf2.predict(X_test)
                    logging.info("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
                    logging.info(str(accuracy_score(predictions,y_test)) + " training-eval configuration with best score (LR,{})".format(dim))
                    acc_lr = scores.mean()
                    writer.writerow(['LR_KG',kg,scores.mean(),scores.std(),accuracy_score(predictions,y_test)])
                    if max(acc_lr,acc_svm) > best:
                        best = max(acc_lr,acc_svm)
                        d = n
    return d

def train(final_texts,emb,final_y,kg,output=False):
    print("KG: "+kg)
    dataframe = build_dataframe(final_texts)
    report = []
    best = 0
    dat_mat = None
    with open('fake_news_tfidf_2svd.tsv', 'a+') as tsvfile:        
        writer = csv.writer(tsvfile, delimiter='\t')
        writer.writerow(['MODEL','KG USED', 'TEST ACC','CV ACC', 'CV STD'])
        for nrep in range(1):
            for nfeat in [2500,5000,10000,15000,17000]:
                for dim in [256,512,768]:
                    tokenizer, feature_names, data_matrix_o = get_features(dataframe, max_num_feat = nfeat, labels = final_y)
                    reducer = TruncatedSVD(n_components = min(dim, nfeat * len(feature_names)-1))
     #               reducer = umap.UMAP(n_components = min(dim, nfeat * len(feature_names)-1))
                    data_matrix = reducer.fit_transform(data_matrix_o)
                    print(data_matrix.shape)
                    X_train, X_test, y_train, y_test = train_test_split(data_matrix, final_y, train_size=0.9, test_size=0.1,random_state = 0)
                    logging.info("Generated {} features.".format(nfeat*len(feature_names)))
               #     parameters = {'kernel':["linear","poly"], 'C':[0.1, 1, 10, 100, 500],"gamma":["scale","auto"],"class_weight":["balanced",None]}
                    parameters = {"loss":["hinge","log"],"penalty":["elasticnet"],"alpha":[0.01,0.001,0.0001,0.0005],"l1_ratio":[0.05,0.25,0.3,0.6,0.8,0.95],"power_t":[0.5,0.1,0.9]}
    #                svc = svm.SVC()
                    svc = SGDClassifier()
                    clf1 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                    scores = cross_val_score(clf1, data_matrix, final_y, cv=5,scoring='accuracy')
                    acc_svm = scores.mean()
                    clf1.fit(X_train, y_train)
                    predictions = clf1.predict(X_test)
                    logging.info("PLAIN SGD Training 5fCV Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
                    logging.info(str(accuracy_score(predictions,y_test)) + " training-eval configuration with best score (SGD,{})".format(dim))
                    reducer = TruncatedSVD(n_components = min(dim, nfeat * (len(feature_names)-1+500)))
     #               reducer = umap.UMAP(n_components = min(dim, nfeat * len(feature_names)-1))
                    data_matrix_1 = np.hstack((data_matrix,emb))
                    writer.writerow(['PLAIN_SGD_'+str(dim)+"_f_"+str(nfeat),kg,scores.mean(),scores.std(),accuracy_score(predictions,y_test)])
                    X_train, X_test, y_train, y_test = train_test_split(data_matrix_1, final_y, train_size=0.9, test_size=0.1 ,random_state = 0)
                    logging.info("Generated {} features.".format(nfeat*len(feature_names)))
               #     parameters = {'kernel':["linear","poly"], 'C':[0.1, 1, 10, 100, 500],"gamma":["scale","auto"],"class_weight":["balanced",None]}
                    parameters = {"loss":["hinge","log"],"penalty":["elasticnet"],"alpha":[0.01,0.001,0.0001,0.0005],"l1_ratio":[0.05,0.25,0.3,0.6,0.8,0.95],"power_t":[0.5,0.1,0.9]}
    #                svc = svm.SVC()
                    svc = SGDClassifier()
                    clf1 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                    score1 = cross_val_score(clf1,  data_matrix, final_y, cv=5,scoring='accuracy')
                    acc_svm = score1.mean()
                    clf1.fit(X_train, y_train)   
                    predictions = clf1.predict(X_test)                     
                    writer.writerow(['SGD_'+str(dim)+"_f_"+str(nfeat),kg,score1.mean(),score1.std(),accuracy_score(predictions,y_test)])
                    logging.info("SGD Training 5fCV Accuracy: %0.2f (+/- %0.2f)" % (score1.mean(), score1.std() * 2))
                    logging.info(str(accuracy_score(predictions,y_test)) + " training-eval configuration with best score (SGD,{})".format(dim))
                    parameters = {"C":[0.1,1,10,25,50,100,500],"penalty":["l2"]}
                    svc = LogisticRegression(max_iter = 100000)
                    clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                    score2 = cross_val_score(clf2, data_matrix, final_y, cv=5,scoring='accuracy') 
                    clf2.fit(X_train, y_train)
                    predictions = clf2.predict(X_test)
                    logging.info("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (score2.mean(), score2.std() * 2))
                    logging.info(str(accuracy_score(predictions,y_test)) + " training-eval configuration with best score (LR,{})".format(dim))
                    acc_lr = scores.mean()
                    writer.writerow(['LR_'+str(dim)+"_f_"+str(nfeat),kg,score2.mean(),score2.std(),accuracy_score(predictions,y_test)])
                    score1 = score1.mean()
                    score2 = score2.mean()
                    if score2 > score1.mean():
                        score1 = score2
                    if score1 > best:
                        best = score1
                        dat_mat = data_matrix_1
    print("BEST CURRENTLY: " + str(best))
    return dat_mat    
tknzr = TweetTokenizer()
X = []
X_t = []
y = []
i = 0
found = 0
not_found = 0
t2i = {}
t2k = {}
        
t2i = {}
total_w = 0
found_w = 0

with open('eng.csv','r',encoding='utf-8') as f:
    readCSV = csv.reader(f, delimiter=',')
    for l in readCSV:
        t = l[0].lower()
        out = remove_stopwords(remove_punctuation(t))
        txt = list(set(tknzr.tokenize(t)))
        X.append(l[0])
        y.append(l[1])
        t2i[i] = txt
        i = i + 1
    for kg in tqdm(glob.glob("./test_env/wikidata/*.pkl")):
        found = 0
        not_found = 0
        print("KG: "+kg)
        with open(kg, "rb") as fin:
            model = pickle.load(fin)
        entity2id = model.graph.entity2id
        entity_embeddings = model.solver.entity_embeddings
        alias2entity = gv.dataset.wikidata5m.alias2entity
        t2k[kg] = {}
        t2e = {}
        for i in t2i:
         txt = t2i[i]
         fnd = []
         cnt = 0
         for w in txt:
             total_w = total_w + 1
             try:
                fnd.append(entity_embeddings[entity2id[alias2entity[w]]])
                cnt = cnt + 1
                found_w = found_w + 1
             except Exception as e:
                 print(e)
         print(len(fnd))
         if cnt != 0:
             t2e[i] = sum(fnd)/cnt
             found = found + 1
         else:
             not_found = not_found + 1
         print(found)
         print(not_found)
         t2k[kg] = t2e
         print(f'FOUND : {found} NOT FOUND: {not_found}')
    pickle.dump(t2k,open(os.path.join(config.pickles,"t2k2.pkl"),"wb"))
t2k = pickle.load(open(os.path.join(config.pickles,"t2k2.pkl"),"rb"))
models = {}
for kg in t2k:
    emb = []
    for i in t2k[kg]:
        emb.append(t2k[kg][i])
    models[kg] = train2(X,np.array((emb)),y,kg)
    
    

with open('fake_news_data2.pkl', 'wb') as f:
     pickle.dump(models,f)