class Book(object):
    def __init__(self, title, body, _copyright, categories, authors, publish_date, isbn, url):
        self.title = title
        self.text = body if not body is None else "empty"
        self._copyright = _copyright
        self.categories = categories        
        self.authors = authors
        self.publish_date = publish_date
        self.isbn = isbn
        self.url = url
        
    def get_task_labels(self, level=0):
        if isinstance(self.categories, dict):
            for topic in self.categories:
                if self.categories[topic]['d'] == level:
                    yield topic
                
    def get_task1_labels(self):
        for topic in self.categories:
            if self.categories[topic]['d'] == 0:
                yield topic

    def get_task2_labels(self):
        for topic in self.categories:
            if self.categories[topic]['d'] == 1:
                yield topic

    
    def to_df_item(self, with_text=True):
        # categories_level = name: level
     
        age_in_years = max(round((datetime.today() - datetime.strptime(self.publish_date, '%Y-%m-%d')).days / 365), 1)
        
        words = self.text.split()
        words_len = np.array([len(w) for w in words])
        
        authors_str = ';'.join(self.authors)
        
        doc = {
            'isbn': self.isbn,
            'title': self.title,
            'title_words': len(self.title.split()),
            'author_count': len(self.authors),
            'authors': ';'.join(self.authors),
            'authors_academic': 1 if 'dr.' in authors_str.lower() or 'prof.' in authors_str.lower() else 0,
            'copyright': self._copyright,
            'publish_date': self.publish_date,
            'age': age_in_years,
            'text_words': len(words),
            'text_words_10': round(len(words) / 10), 
            'word_len_mean': round(np.mean(words_len)),
            'word_len_median': round(np.median(words_len)),
            'word_len_max': np.max(words_len),           
            
        }
        
        if with_text:
            doc['text'] = self.text
        
        return doc
    
    
    def to_df_item_labels(self, categories_level):           
        labels = {('-' * level) + label: 0 for label, level in categories_level.items()}  # init with 0
        for cat, v in self.categories.items():
            k = ('-' * v['d']) + cat            
            if k in labels: 
                labels[k] = 1
        
        return labels