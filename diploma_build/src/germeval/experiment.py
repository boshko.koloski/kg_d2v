# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 09:59:34 2020

@author: daskalot
"""
from sklearn.metrics import f1_score
import pickle
import os
import pandas as pd
import numpy as np
import config
from parse_data import *
from book import Book
import numpy as np
import pandas as pd
from tqdm import tqdm
from transformers import BertTokenizer, BertModel
from simpletransformers.classification import ClassificationModel
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

def buildXY(books):
    X = []
    y = []
    for b in books:
        for t in b.get_task1_labels():
            y.append(t)
            break
        X.append(b.text)
    le = preprocessing.LabelEncoder()
    le.fit(list(set(y)))
    y_ = le.transform(y)
    assert len(y_) == len(X)
    X_ = []
    X_train, X_test, y_train, y_test = train_test_split(X, y_, test_size=0.2, random_state=1)
    for i in range(len(X_train)):
        X_.append([X_train[i],y_train[i]])
    train_df = pd.DataFrame(X_)
    X_= []
    for i in range(len(X_test)):
        X_.append([X_test[i],y_test[i]])
    eval_df = pd.DataFrame(X_)
    return le, train_df, eval_df

def testXY(books,le):
    X = []
    y = []
    for b in books:
        for t in b.get_task1_labels():
            y.append(t)
            break
        X.append(b.text)
    le = preprocessing.LabelEncoder()
    le.fit(list(set(y)))
    y_ = le.transform(y)
    assert len(y_) == len(X)   
    test_df = X
    print(test_df)
    return test_df, y_

"""        
def train(train_df):
    MODEL_TYPE = 'bert-base-multilingual-cased'
    MAX_SIZE = 150
    BATCH_SIZE = 200
    tokenizer = BertTokenizer.from_pretrained(MODEL_TYPE)
    model = BertModel.from_pretrained(MODEL_TYPE)
    tokenized_input = train_df['text'].apply((lambda x: tokenizer.encode(x, add_special_tokens=True)))
    padded_tokenized_input = [[i + [0]*(MAX_SIZE-len(i)) for i in tokenized_input.values]]
    padded_tokenized_input = np.array(padded_tokenized_input)
    attention_masks  = np.where(padded_tokenized_input != 0, 1, 0)
    input_ids = torch.tensor(padded_tokenized_input)  
    attention_masks = torch.tensor(attention_masks)
    all_train_embedding = []
    with torch.no_grad():
        for i in tqdm(range(0,len(input_ids),200)):    
            last_hidden_states = model(input_ids[i:min(i+200,len(train_df))], attention_mask = attention_masks[i:min(i+200,len(train_df))])[0][:,0,:].numpy()
            all_train_embedding.append(last_hidden_states)
    unbatched_train = []
    for batch in all_train_embedding:
        for seq in batch:
            unbatched_train.append(seq)
    train_labels = train_df['target']
"""    

def train(le, train_df, eval_df, test_df,y_test):
    #le, train_df, eval_df    
    f = open("baselines.txt","w")
    for m in ['bert-base-multilingual-cased','bert-base-german-cased','bert-base-german-dbmdz-cased']: 
        model = ClassificationModel('bert', m, num_labels=8, args={'reprocess_input_data': True, 'overwrite_output_dir': True})
        model.train_model(train_df)
        result, model_outputs, wrong_predictions = model.eval_model(eval_df)
        print(result)
        predictions, raw_outputs = model.predict(test_df)
        print(f1_score(y_test, predictions, average='macro'))
        f.write("MODEL: "+m+" "+"F1:"+str(f1_score(y_test, predictions, average='macro'))+"\n")
    f.close()
books, task_labels, authors = parseXML(config.data['train'])
le, train_df, eval_df = buildXY(books)
books, task_labels, authors = parseXML(config.data['test'])
test_df,y_test = testXY(books, le) 
train(le, train_df, eval_df,test_df,y_test)