from sentence_transformers import SentenceTransformer, LoggingHandler
import numpy as np
from sklearn.metrics import f1_score
import pickle
import os
import pandas as pd
import numpy as np
import config
from parse_data import *
from book import Book
import numpy as np
import pandas as pd
from tqdm import tqdm
from transformers import BertTokenizer, BertModel
from simpletransformers.classification import ClassificationModel
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

def buildXY(books):
    X = []
    y = []
    for b in books:
        for t in b.get_task1_labels():
            y.append(t)
            break
        X.append(b.text)
    le = preprocessing.LabelEncoder()
    le.fit(list(set(y)))
    y_ = le.transform(y)
    assert len(y_) == len(X)
    X_ = []
    X_train, X_test, y_train, y_test = train_test_split(X, y_, test_size=0.2, random_state=1)
    for i in range(len(X_train)):
        X_.append([X_train[i],y_train[i]])
    train_df = pd.DataFrame(X_)
    X_= []
    for i in range(len(X_test)):
        X_.append([X_test[i],y_test[i]])
    eval_df = pd.DataFrame(X_)
    return le, train_df, eval_df

def testXY(books,le):
    X = []
    y = []
    for b in books:
        for t in b.get_task1_labels():
            y.append(t)
            break
        X.append(b.text)
    le = preprocessing.LabelEncoder()
    le.fit(list(set(y)))
    y_ = le.transform(y)
    assert len(y_) == len(X)   
    test_df = X
    print(test_df)
    return test_df, y_


np.set_printoptions(threshold=100)
model = SentenceTransformer('bert-base-nli-mean-tokens')
sentences = ['This framework generates embeddings for each input sentence',
             'Sentences are passed as a list of string.',
             'The quick brown fox jumps over the lazy dog.']
sentence_embeddings = model.encode(sentences)
# The result is a list of sentence embeddings as numpy arrays
for sentence, embedding in zip(sentences, sentence_embeddings):
    print("Sentence:", sentence)
    print("Embedding:", embedding)
    print("")

books, task_labels, authors = parseXML(config.data['train'])
docs = getBooks(books)

