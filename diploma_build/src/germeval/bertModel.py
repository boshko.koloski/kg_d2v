from book import Book
import pickle
import os
import csv
from feature_construction import *
from sklearn import preprocessing #import MultiLabelBinarizer
from parse_data import *
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer 
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
import numpy as np
import config
import nltk
from nltk.tokenize import sent_tokenize
def getBooks(books):
    mat = {}
    for b in books:
        mat[b.isbn] = b.text
    return mat

def labels1(books):
    mat = {}
    lbls = set()
    for b in books:
        mat[b.isbn] = []
        for x in b.get_task_labels():
            mat[b.isbn].append(x)#x[0].rstrip())
            break
    print(len(lbls))
    return mat

def clean_title(name):
    parts = name.split(" ")
    titles = ['dr.','mr.','prof.']
    for i in range(len(parts)):
        for title in titles:
            if parts[i] == title:
                parts[i] = ""
                break
    return " ".join(parts)

def reverse_name(author):
    parts = author.split(" ")
    parts[-1],parts[0] = parts[0],parts[-1]
    return " ".join(parts)


def train(X,labels,books,documents):
    models = {}
    with open('bert_records_b.tsv', 'w') as tsvfile:        
        writer = csv.writer(tsvfile, delimiter='\t')
        writer.writerow(['TASK NAME','KG USED', 'MEAN f1', 'STD'])
        nf = 0
        found = 0
        b2k = {}
        a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))
        for book in books:
            a2e = []
            for author in book.authors:
                queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                for query in queries:
                    if query in a2k:
                        a2e = a2k[query]
                        break
                if len(a2e) != 0:
                    break
            if len(a2e) != 0:
                b2k[book.isbn] = a2e
                found=found+1    
            else:
                nf = nf + 1
        a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))    
        f = open("bert_baseline.txt","w")
        ff = False
        for kg in a2k:
            nf = 0
            found = 0
            b2k = {}
            for book in books:
                a2e = []
                for author in book.authors:
                    queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                    for query in queries:
                        if query in set(a2k[kg]):
                            a2e = a2k[kg][query]
                            break
                    if len(a2e) != 0:
                        break
                if len(a2e) != 0:
                    b2k[book.isbn] = a2e
                    found=found+1    
                else:
                    nf = nf + 1
            print(f'Embeddings found {found} not found {nf}')
            m_emb = []
            m_lbls = []
            mat_all = []
            m_fixed = []
            for i,book in enumerate(documents):
                if book in b2k:
                    m_emb.append(b2k[book])
                    m_fixed.append(X[i][:])
                    m_lbls.append(labels[book])
            m_emb = np.array((m_emb))
            m_fixed = np.array((m_fixed))
            m_enriched = np.concatenate((m_emb,m_fixed),axis=1)
            m_lbls = np.array((m_lbls))
            m_lbls = np.ravel(m_lbls)        
            print(m_fixed.shape)
            np.random.seed(0)
            if not ff:
                dim = 512
                X_train, X_test, y_train, y_test = train_test_split(m_fixed, np.array((m_lbls)), train_size=0.9, test_size=0.1, random_state=0)            
                parameters = {"C":[0.1,1,10],"penalty":["l2"]}
                svc = LogisticRegression(max_iter = 100000)
                clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = -1,cv = 10, refit = True)
                scores = cross_val_score(clf2, X_train, y_train, cv=5,scoring='f1_macro') 
                clf2.fit(X_train, y_train)
                predictions = clf2.predict(X_test)
                print("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
                print(str(f1_score(predictions,y_test,average='weighted')) + " training-eval configuration with best score (LR,{})".format(dim))
                acc_svm = scores.mean() 
                print(f'F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
                ff = True
                f.write(f'F1 MEAN: {acc_svm}, STD: {np.std(np.array((scores)))}\n')                
                writer.writerow(['task_b_germ_evl','none', acc_svm, scores.std() * 2])
                models['baseline'] = clf2
            np.random.seed(0)
            dim = 512*2
            X_train, X_test, y_train, y_test = train_test_split(m_enriched, np.array((m_lbls)), train_size=0.9, test_size=0.1, random_state=0)
            parameters = {"C":[0.1,1,10],"penalty":["l2"]}
            svc = LogisticRegression(max_iter = 100000)
            clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = -1,cv = 10, refit = True)
            scores = cross_val_score(clf2, X_train, y_train, cv=5,scoring='f1_macro') 
            clf2.fit(X_train, y_train)
            predictions = clf2.predict(X_test)
            print("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
            print(str(f1_score(predictions,y_test,average='weighted')) + " training-eval configuration with best score (LR,{})".format(dim))
            acc_svm = scores.mean() 
            print(f'KG: {kg} F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
            ff = True
            f.write(f'KG: {kg} F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
            writer.writerow(['task_b_germ_evl',kg, acc_svm, scores.std() * 2])
            models[kg] = clf2
    with open('bert_train_models_b.pkl', 'w') as f:
         pickle.dump(models,f)
        
books, task_labels, authors = parseXML(config.data['train'])
lbls = labels1(books)
docs = getBooks(books)
b2k = pickle.load(open(os.path.join(config.pickles,"b2multbook.pkl"),'rb'))    
X = []
y = []
for x in b2k:
    X.append(b2k[x])
    y.append(lbls[x])
X = np.array((X))
print(X.shape)
train(X,lbls,books,docs)
