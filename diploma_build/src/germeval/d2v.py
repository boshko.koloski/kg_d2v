import os
from parse_data import *
from gensim import utils
import gensim.parsing.preprocessing as gsp
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from sklearn.base import BaseEstimator
from sklearn import utils as skl_utils
from tqdm import tqdm
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from sklearn.base import BaseEstimator
from sklearn import utils as skl_utils
from tqdm import tqdm
import multiprocessing
import numpy as np
import pickle
from sklearn import svm
from sklearn.datasets import load_linnerud
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
import config
from book import Book
import pickle
import os
from feature_construction import *
from parse_data import *
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
def readListDir(path):
    "Returns list of articles present in the FS."
    return os.listdir(path)

def preprocess(s):
    filters = [
           gsp.strip_tags, 
           gsp.strip_punctuation,
           gsp.strip_multiple_whitespaces,
           gsp.strip_numeric,
           gsp.remove_stopwords, 
           gsp.strip_short, 
           gsp.stem_text
    ]
    s = s.lower()
    s = utils.to_unicode(s)
    for f in filters:
        s = f(s)
    return s

def getBooks(books):
    mat = {}
    for b in books:
        mat[b.isbn] = preprocess(b.text)
    return mat
    
def labels1(books):
    mat = {}
    for b in books:
        mat[b.isbn] = []
        for x in b.get_task1_labels():
            mat[b.isbn].append(x)
            break
    return mat
    
def export(model_,model_name):
    with open(os.path.join(config.pickles,model_name),mode='wb') as f:
        pickle.dump(model_,f)

def clean_title(name):
    parts = name.split(" ")
    titles = ['dr.','mr.','prof.']
    for i in range(len(parts)):
        for title in titles:
            if parts[i] == title:
                parts[i] = ""
                break
    return " ".join(parts)

def reverse_name(author):
    parts = author.split(" ")
    parts[-1],parts[0] = parts[0],parts[-1]
    return " ".join(parts)

class Doc2VecTransformer(BaseEstimator):
    def __init__(self,docs, vector_size=100, learning_rate=0.02, epochs=20):
        self.docs = docs
        self.learning_rate = learning_rate
        self.epochs = epochs
        self._model = None
        self.vector_size = vector_size
        self.workers = multiprocessing.cpu_count() - 1
    def fit(self):
        tagged_x = []
        for doc in docs:
            temp_doc = TaggedDocument(docs[doc].split(),[doc])
            tagged_x.append(temp_doc)            
        model = Doc2Vec(documents=tagged_x, vector_size=self.vector_size, workers=self.workers)
        for epoch in range(self.epochs):
            model.train(skl_utils.shuffle([x for x in tqdm(tagged_x)]), total_examples=len(tagged_x), epochs=1)
            model.alpha -= self.learning_rate
            model.min_alpha = model.alpha
        self._model = model
        return self

    def transform(self):
       return np.asmatrix(np.array([self._model.infer_vector(docs[doc].split()) for doc in self.docs]))
    
    def doc2feature(self,doc):
       return np.array(self._model.infer_vector(self.docs[doc].split()))

    def _export(self,path="..\models\d2vmodel.pkl"):
        self._model.save(path)

    def _import(self,path="..\models\d2vmodel.pkl"):
        self._model = pickle.load(open(path,'rb'))

import csv
#TO DO:
#Change path addressing 
#Add it to make features also for TC task
#Cast doc2feature only once to speed up
def buildX(doc2vec,docs):
    """Builds features and target variables"""
    X = []
    for doc in docs:
        x = doc2vec.doc2feature(doc)
        X.append(x)        
    return np.array((X))

def train(X,y,books,documents): 
    models = {}
    with open('d2v-256.tsv', 'w') as tsvfile:        
        writer = csv.writer(tsvfile, delimiter='\t')
        writer.writerow(['TASK NAME','KG USED', 'MEAN f1', 'STD'])
        nf = 0
        found = 0
        b2k = {}
        a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))
        for book in books:
            a2e = []
            for author in book.authors:
                queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                for query in queries:
                    if query in a2k:
                        a2e = a2k[query]
                        break
                if len(a2e) != 0:
                    break
            if len(a2e) != 0:
                b2k[book.isbn] = a2e
                found=found+1    
            else:
                nf = nf + 1
        a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))    
        f = open("bert_baseline.txt","w")
        ff = False
        for kg in a2k:
            nf = 0
            found = 0
            b2k = {}
            for book in books:
                a2e = []
                for author in book.authors:
                    queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                    for query in queries:
                        if query in set(a2k[kg]):
                            a2e = a2k[kg][query]
                            break
                    if len(a2e) != 0:
                        break
                if len(a2e) != 0:
                    b2k[book.isbn] = a2e
                    found=found+1    
                else:
                    nf = nf + 1
            print(f'Embeddings found {found} not found {nf}')
            m_emb = []
            m_lbls = []
            mat_all = []
            m_fixed = []
            for i,book in enumerate(documents):
                if book in b2k:
                    m_emb.append(b2k[book])
                    m_fixed.append(X[i][:])
                    m_lbls.append(labels[book])
            m_emb = np.array((m_emb))
            m_fixed = np.array((m_fixed))
            m_enriched = np.concatenate((m_emb,m_fixed),axis=1)
            m_lbls = np.array((m_lbls))
            m_lbls = np.ravel(m_lbls)        
            print(m_fixed.shape)
            np.random.seed(0)
            if not ff:
                dim = 512
                X_train, X_test, y_train, y_test = train_test_split(m_fixed, np.array((m_lbls)), train_size=0.9, test_size=0.1, random_state=0)            
                parameters = {"C":[0.1,1,10],"penalty":["l2"]}
                svc = LogisticRegression(max_iter = 100000)
                clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = -1,cv = 10, refit = True)
                scores = cross_val_score(clf2, X_train, y_train, cv=5) 
                clf2.fit(X_train, y_train)
                predictions = clf2.predict(X_test)
                print("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
                print(str(f1_score(predictions,y_test,average='weighted')) + " training-eval configuration with best score (LR,{})".format(dim))
                acc_svm = scores.mean() 
                print(f'F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
                ff = True
                f.write(f'F1 MEAN: {acc_svm}, STD: {np.std(np.array((scores)))}\n')                
                writer.writerow(['task_a_germ_evl','none', acc_svm, scores.std() * 2])
                models['baseline'] = clf2
            np.random.seed(0)
            dim = 512*2
            X_train, X_test, y_train, y_test = train_test_split(m_enriched, np.array((m_lbls)), train_size=0.9, test_size=0.1, random_state=0)
            parameters = {"C":[0.1,1,10],"penalty":["l2"]}
            svc = LogisticRegression(max_iter = 100000)
            clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = -1,cv = 10, refit = True)
            scores = cross_val_score(clf2, X_train, y_train, cv=5) 
            clf2.fit(X_train, y_train)
            predictions = clf2.predict(X_test)
            print("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
            print(str(f1_score(predictions,y_test,average='weighted')) + " training-eval configuration with best score (LR,{})".format(dim))
            acc_svm = scores.mean() 
            print(f'KG: {kg} F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
            ff = True
            f.write(f'KG: {kg} F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
            writer.writerow(['task_a_germ_evl',kg, acc_svm, scores.std() * 2])
            models[kg] = clf2
            
if __name__ == '__main__':
    books, task_labels, authors = parseXML(config.data['train'])
    docs = getBooks(books)
    labels = labels1(books)
    doc2vec = Doc2VecTransformer(docs,vector_size=256)
    doc2vec.fit()
    X = buildX(doc2vec,docs)
    train(X,labels,books,docs)
    

    