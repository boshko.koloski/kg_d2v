from book import Book
import pickle
import os
from feature_construction import *
from parse_data import *
from sklearn import preprocessing
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sentence_transformers import SentenceTransformer, LoggingHandler
import numpy as np
import config
import nltk
from nltk.tokenize import sent_tokenize

def getBooks(books):
    mat = []
    for b in books:
        mat.append(b.text)
    return mat
    
def labels1(books):
    mat = {}
    for b in books:
        mat[b.isbn] = []
        for x in b.get_task_labels():
            mat[b.isbn].append(x)
            break
    return mat
    
books, task_labels, authors = parseXML(config.data['train'])
docs = getBooks(books)
np.set_printoptions(threshold=100)
model = SentenceTransformer('distiluse-base-multilingual-cased')
books, task_labels, authors = parseXML(config.data['train'])
b2bert = {}
for b in books:
    text = " .".join[b.title,b.text]
    make_sent = sent_tokenize(text, language='german')
    sentence_embeddings = np.mean(model.encode(make_sent))
    b2bert[b.isbn] = sentence_embeddings
    
with open(os.path.join(config.pickles,"b2multbook.pkl"),"wb") as f:
    pickle.dump(b2bert,f)
