import os
import glob
import pickle
from sklearn.metrics import f1_score
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import cross_val_score

import numpy as np

def _import(fname):
    with open(fname,"rb") as f:
        return pickle.load(f)

def buildXY():
    kg = _import("c2k.pkl")
    data = open("churn.csv","r")
    scaler = StandardScaler()
    for cnt2emb in kg:
        y = []
        X = []
        XX = []
        cnt2emb = kg[cnt2emb]
        #print(cnt2emb.keys())
        l = 0
        data = open("churn.csv","r") 
        for line in data:
            if not l == 0:        
                x = []
                #CS
                line = line.split(",")
                x.append(float(line[3]))
                y.append(line[-1].rstrip())                
                #add country        
                #GENDER
                if line[5] == "Male":
                    x.append(1)
                else:
                    x.append(0)
                #age
                for i in range(6,13):
                    x.append(float(line[i]))
                #print(x[0:12])
                XX.append(x)
                cnt = cnt2emb[line[4].lower()]
                x = x + cnt.tolist()
                X.append(x)
            #print(X)
            #print(y)
            l = l + 1
        
        X = np.array((X))
        scaler.fit(X)
        y = np.array((y))
        XX = np.array((XX))
        #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        reg = LogisticRegression()#.fit(X_train, y_train)
        scores = cross_val_score(reg, X, y, cv=3)
        print("Embeddings added %d -> Accuracy: %0.2f (+/- %0.2f)" % (X.shape[1],scores.mean(), scores.std())) 
        reg2 = LogisticRegression()#.fit(X_train, y_train)
        scores = cross_val_score(reg2, XX, y, cv=3)
        print("Without embeddings %d -> Accuracy: %0.2f (+/- %0.2f)" % (XX.shape[1],scores.mean(), scores.std()))        
buildXY()
#print("TETE")
#kg = _import("pickls/a2k.pkl")
#for k in kg:
#    print(kg[k].keys())