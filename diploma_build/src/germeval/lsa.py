from book import Book
import pickle
import os
from feature_construction import *
from parse_data import *
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
import numpy as np
import config
import csv
def getBooks(books):
    mat = {}
    for b in books:
        mat[b.isbn] = b.text
    return mat
    
def labels1(books):
    mat = {}
    for b in books:
        mat[b.isbn] = []
        for x in b.get_task_labels():
            mat[b.isbn].append(x)
            break
    return mat
    
def export(model_,model_name):
    with open(os.path.join(config.pickles,model_name),mode='wb') as f:
        pickle.dump(model_,f)

def clean_title(name):
    parts = name.split(" ")
    titles = ['dr.','mr.','prof.']
    for i in range(len(parts)):
        for title in titles:
            if parts[i] == title:
                parts[i] = ""
                break
    return " ".join(parts)

def reverse_name(author):
    parts = author.split(" ")
    parts[-1],parts[0] = parts[0],parts[-1]
    return " ".join(parts)

def train(X,y,books,documents): 
    models = {}
    with open('tf_idf-256.tsv', 'w') as tsvfile:        
        writer = csv.writer(tsvfile, delimiter='\t')
        writer.writerow(['TASK NAME','KG USED', 'MEAN f1', 'STD'])
        nf = 0
        found = 0
        b2k = {}
        a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))
        for book in books:
            a2e = []
            for author in book.authors:
                queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                for query in queries:
                    if query in a2k:
                        a2e = a2k[query]
                        break
                if len(a2e) != 0:
                    break
            if len(a2e) != 0:
                b2k[book.isbn] = a2e
                found=found+1    
            else:
                nf = nf + 1
        a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))    
        f = open("tf_idf.txt","w")
        ff = False
        for kg in a2k:
            nf = 0
            found = 0
            b2k = {}
            for book in books:
                a2e = []
                for author in book.authors:
                    queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                    for query in queries:
                        if query in set(a2k[kg]):
                            a2e = a2k[kg][query]
                            break
                    if len(a2e) != 0:
                        break
                if len(a2e) != 0:
                    b2k[book.isbn] = a2e
                    found=found+1    
                else:
                    nf = nf + 1
            print(f'Embeddings found {found} not found {nf}')
            m_emb = []
            m_lbls = []
            mat_all = []
            m_fixed = []
            for i,book in enumerate(documents):
                if book in b2k:
                    m_emb.append(b2k[book])
                    m_fixed.append(X[i][:])
                    m_lbls.append(labels[book])
            m_emb = np.array((m_emb))
            m_fixed = np.array((m_fixed))
            m_enriched = np.concatenate((m_emb,m_fixed),axis=1)
            m_lbls = np.array((m_lbls))
            m_lbls = np.ravel(m_lbls)        
            print(m_fixed.shape)
            np.random.seed(0)
            dim = 512
            X_train, X_test, y_train, y_test = train_test_split(m_emb, np.array((m_lbls)), train_size=0.9, test_size=0.1, random_state=0)            
            parameters = {"C":[0.1,1,10],"penalty":["l2"]}
            svc = LogisticRegression(max_iter = 100000)
            clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = -1,cv = 10, refit = True)
            scores = cross_val_score(clf2, X_train, y_train, cv=5) 
            clf2.fit(X_train, y_train)
            predictions = clf2.predict(X_test)
            print("LR 5fCV Training ccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
            print(str(f1_score(predictions,y_test,average='weighted')) + " training-eval configuration with best score (LR,{})".format(dim))
            acc_svm = scores.mean() 
            print(f'F1 MEAN: {acc_svm}, STD: {scores.std() * 2}\n')
            ff = True
            f.write(f'F1 MEAN: {acc_svm}, STD: {np.std(np.array((scores)))}\n')                
            writer.writerow(['task_a_germ_evl','auth_only_'+kg, acc_svm, scores.std() * 2])
            models['baseline'] = clf2

            
            
books, task_labels, authors = parseXML(config.data['train'])
documents = getBooks(books)
labels = labels1(books)
#dataframe = build_dataframe(documents)
#export(dataframe,"dataframe_TF.plk")
#print('Dataframe built')
dataframe = build_dataframe(documents)
nfeat = 5000
dim = 256
tokenizer, feature_names, data_matrix = get_features(dataframe, max_num_feat = nfeat, labels = None)
reducer = TruncatedSVD(n_components = min(dim, nfeat * len(feature_names)-1))
X = reducer.fit_transform(data_matrix)
#export(tokenizer,"tokenizer.pkl")
#export(reducer,"reducer.pkl")
#export(data_matrix,"data_matrix.pkl")
#tokenizer = pickle.load(open(os.path.join(config.pickles,"tokenizer.pkl"),'rb'))
#reducer = pickle.load(open(os.path.join(config.pickles,"reducer.pkl"),'rb'))
#X = pickle.load(open(os.path.join(config.pickles,"data_matrix.pkl"),'rb'))
train(X,labels,books,documents)