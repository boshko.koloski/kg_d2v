from book import Book
import pickle
import os
from torch import nn, optim
import torch.nn.functional as F
import torch.utils.data as data
import torch
from feature_construction import *
from parse_data import *
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.metrics import matthews_corrcoef
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
import numpy as np
import config
import nltk
from nltk.tokenize import sent_tokenize
def getBooks(books):
    mat = {}
    for b in books:
        mat[b.isbn] = b.text
    return mat

def labels1(books):
    mat = {}
    for b in books:
        mat[b.isbn] = []
        for x in b.get_task_labels():
            mat[b.isbn].append(x)
            break
    return mat

def clean_title(name):
    parts = name.split(" ")
    titles = ['dr.','mr.','prof.']
    for i in range(len(parts)):
        for title in titles:
            if parts[i] == title:
                parts[i] = ""
                break
    return " ".join(parts)

def reverse_name(author):
    parts = author.split(" ")
    parts[-1],parts[0] = parts[0],parts[-1]
    return " ".join(parts)

class Classifier(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(1024, 512) 
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, 128)
        self.fc4 = nn.Linear(128, 10)
        self.dropout = nn.Dropout(p=0.2)
        
    def forward(self, x):
        x = x.view(x.shape[0], -1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.dropout(F.relu(self.fc3(x)))
        x = F.log_softmax(self.fc4(x), dim=1)
        return x

def train(X,labels,books,documents,tsv="asdas"):
    nf = 0
    found = 0
    b2k = {}
    a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))
    for book in books:
        a2e = []
        for author in book.authors:
            queries = [author, author.lower(), clean_title(author), reverse_name(author)]
            for query in queries:
                if query in a2k:
                    a2e = a2k[query]
                    break
            if len(a2e) != 0:
                break
        if len(a2e) != 0:
            b2k[book.isbn] = a2e
            found=found+1    
        else:
            nf = nf + 1
    a2k = pickle.load(open(os.path.join(config.pickles,"a2k.pkl"),'rb'))    
    f = open("bert_baseline.txt","w")
    ff = False
    for kg in a2k:
        nf = 0
        found = 0
        b2k = {}
        for book in books:
            a2e = []
            for author in book.authors:
                queries = [author, author.lower(), clean_title(author), reverse_name(author)]
                for query in queries:
                    if query in set(a2k[kg]):
                        a2e = a2k[kg][query]
                        break
                if len(a2e) != 0:
                    break
            if len(a2e) != 0:
                b2k[book.isbn] = a2e
                found=found+1    
            else:
                nf = nf + 1
        print(f'Embeddings found {found} not found {nf}')
        m_emb = []
        m_lbls = []
        mat_all = []
        m_fixed = []
        for i,book in enumerate(documents):
            if book in b2k:
                m_emb.append(b2k[book])
                m_fixed.append(X[i][:])
                m_lbls.append(labels[book])
        m_emb = np.array((m_emb))
        m_fixed = np.array((m_fixed))
        m_enriched = np.concatenate((m_emb,m_fixed),axis=1)
        m_lbls = np.array((m_lbls))
        m_lbls = np.ravel(m_lbls)    
        X_train, X_test, y_train, y_test = train_test_split(m_enriched, np.array((m_lbls)), train_size=0.9, test_size=0.1, random_state=0)
        model = Classifier()
        criterion = nn.NLLLoss()
        optimizer = optim.Adam(model.parameters(), lr=0.003)
        from torch.autograd import Variable
        for epoch in range(20):
            for i, (images, labels) in enumerate(trainloader):
                images = Variable(images)
                labels = Variable(labels)
                
                optimizer.zero_grad()
                outputs = model(images)
                
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()
                
                if (i+1) % 100 == 0:
                    print ('Epoch [%d/%d], Iter [%d] Loss: %.4f' %(epoch+1, 10, i+1, loss.data[0]))
        model.eval()
        test = pd.read_csv("../input/test.csv")
        test = test.values.reshape(28000,1,28,28).astype(float)
        test = Variable(torch.Tensor(test))

        pred = model(test)
        
        _, predlabel = torch.max(pred.data, 1)
        predlabel = predlabel.tolist()
        
        predlabel = pd.DataFrame(predlabel)
        predlabel.index = np.arange(28000) + 1
        id = np.arange(28000) + 1
        id = pd.DataFrame(id)
        id.index = id.index + 1
        
        predlabel = pd.concat([id,predlabel], axis=1)
        predlabel.columns = ["ImageId", "Label"]
        
        predlabel.to_csv('predict.csv', index= False)
        acc_svm = f1_score(predictions,y_test, average='macro')
        f.write("ENRICHED MODEL: " + kg + " F1:" + str(acc_svm)+"\n")
        print(acc_svm)
        
import csv

out_file =  open('bert_output.tsv', 'wt') 
tsv_writer = csv.writer(out_file, delimiter='\t')
tsv_writer.writerow(['task','vectorization', 'knowledge_graph', '#features','f1_score','matthews'])

        
books, task_labels, authors = parseXML(config.data['train'])
lbls = labels1(books)
docs = getBooks(books)
b2k = pickle.load(open(os.path.join(config.pickles,"b2multbook.pkl"),'rb'))    
X = []
y = []
for x in b2k:
    X.append(b2k[x])
    print(b2k[x].shape)
    y.append(lbls[x])
X = np.array((X))
print(X.shape)
train(X,lbls,books,docs)
