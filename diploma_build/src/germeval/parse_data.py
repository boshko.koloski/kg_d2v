import codecs
import re
from collections import defaultdict
from lxml import etree
from book import Book
import pickle
import config
import os
def parseXML(xml, task_labels=dict()):  # task_labels = set(), task2_labels = set
    #global alllabels
    #global taks1labels
    authors = list()
    tree = None
    xmlParser = etree.XMLParser(strip_cdata=False, resolve_entities=False, encoding='utf-8', remove_comments=True)
    try:
        tree = etree.parse(xml, parser=xmlParser).getroot()
    except etree.XMLSyntaxError:
        # it wasn't proper xml; wrap tags around it
        lines = codecs.open(xml, 'r',encoding = 'utf-8').readlines()
        lines = ['<rootnode>'] + lines + ['</rootnode>']
        xmlstring = re.sub('&', '&amp;', '\n'.join(lines))
        tree = etree.fromstring(xmlstring)

    maxNrAuthors = 0
    #global maxNrAuthors

    books = []
    for book in tree:
        title, body, _cr, auths, pubdata, isbn, url = ['_'] * 7
        cats = defaultdict(lambda: defaultdict(str))
        
        for node in book:
            if node.tag == 'title':
                title = node.text
            elif node.tag == 'body':
                body = node.text
            elif node.tag == 'copyright':
                _cr = node.text
            elif node.tag == 'categories':
                for subnode in node:
                    # topic d = 0
                    for ssubnode in subnode:
                        # topic d = 1
                        if ssubnode.tag == 'topic':
                            cats[ssubnode.text]['d'] = int(ssubnode.get('d'))
                            if 'label' in ssubnode.attrib:
                                cats[ssubnode.text]['label'] = ssubnode.get('label')
            elif node.tag == 'authors':
                if node.text:  # was empty for some...
                    auths = [x.strip() for x in node.text.split(',')]
                    maxNrAuthors = max(len(auths),maxNrAuthors)    
                    authors = authors + auths
            elif node.tag == 'published':
                pubdata = node.text
            elif node.tag == 'isbn':
                isbn = node.text
            elif node.tag == 'url':
                url = node.text                
        b = Book(title, body, _cr, cats, auths, pubdata, isbn, url)     
        task_labels.update({label: 0 for label in b.get_task_labels(0)})      
        # task2
        task_labels.update({label: 1 for label in b.get_task_labels(1)})
        task_labels.update({label: 2 for label in b.get_task_labels(2)})     
        books.append(b)
        
    authors = set(authors)
    return books, task_labels, authors

def export(obj,fname,fout):
    print("Exporting " + fname)
    with open(os.path.join(fout,fname),mode='wb') as f:
        pickle.dump(obj,f)        

        
def parse_export(path_in,path_out="./pickls"):
    books, task_labels, authors = parseXML(path_in)
    export(authors,"authors.pkl",path_out)
    export(books,"books.pkl",path_out)
    print("Export succesfull")

    
if __name__ == "__main__":
    parse_export(config.data['train'])