

# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 10:40:34 2020

@author: daskalot

"""

import config
from tqdm import tqdm
import graphvite as gv
import os
import pickle
import glob

def getAuthors(books):
    mat = {}
    for b in books:
        mat.append(b.authors)
    return mat

def clean_title(name):
    parts = name.split(" ")
    titles = ['dr.','mr.','prof.']
    for i in range(len(parts)):
        for title in titles:
            if parts[i] == title:
                parts[i] = ""
                break
    return " ".join(parts)

def reverse_name(author):
    parts = author.split(" ")
    parts[-1],parts[0] = parts[0],parts[-1]
    return " ".join(parts)

authors = []

with open("cnts.txt","r") as f:
    for line in f:
        authors.append(line)

books, task_labels, authors = parseXML(config.data['test'])
auth
        
pickeled = False
left_ = []
author2kg = {}
for kg in tqdm(glob.glob("./test_env/wikidata/*.pkl")):
    print("KG: "+kg)
    with open(kg, "rb") as fin:
        model = pickle.load(fin)
    entity2id = model.graph.entity2id
    relation2id = model.graph.relation2id
    entity_embeddings = model.solver.entity_embeddings
    relation_embeddings = model.solver.relation_embeddings
    alias2entity = gv.dataset.wikidata5m.alias2entity
    alias2relation = gv.dataset.wikidata5m.alias2relation
    found = 0
    not_found = 0
    author2kg[kg] = {}
    author2embedding = {}
    print(len(authors))
    for cnt in authors:
         try:
            author2embedding[cnt] = entity_embeddings[entity2id[alias2entity[cnt]]]
         except:
            pass 
    print(len(author2embedding[cnt].keys()))
    author2kg[kg] = author2embedding
    
pickle.dump(author2kg,open(os.path.join(config.pickles,"a2ktest.pkl"),"wb"))
