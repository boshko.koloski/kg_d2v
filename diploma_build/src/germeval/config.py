#INITS
data = dict()
#PATH MODELS
path_models = "../models"
data['dev'] = "./data/blurbs_dev.txt"
data['dev_labeled'] = "./data/blurbs_dev_label.txt"
data['dev_raw'] = "./data/blurbs_dev_nolabel.txt"
#TEST
data['test'] = "./data/blurbs_test.txt"
data['test_label'] = "./data/blurbs_test_label.txt"
data['test_raw'] = "./data/blurbs_test_nolabel.txt"
#DEV
data['train'] = "./data/blurbs_train.txt"
data['train_label'] = "./data/blurbs_train_label.txt"
#
pickles = "./pickls"