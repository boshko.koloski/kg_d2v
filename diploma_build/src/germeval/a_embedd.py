
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 10:40:34 2020

@author: daskalot

"""

import config
from tqdm import tqdm
import graphvite as gv
import os
import pickle
import glob
with open(os.path.join(config.pickles,"author2qid.pkl"), "rb") as fin:
    author2qid = pickle.load(fin)
author2kg = {}
for kg in tqdm(glob.glob("./test_env/wikidata/*.pkl")):
	print("KG: "+kg)
	with open(kg, "rb") as fin:
	    model = pickle.load(fin)
	entity2id = model.graph.entity2id
	relation2id = model.graph.relation2id
	entity_embeddings = model.solver.entity_embeddings
	relation_embeddings = model.solver.relation_embeddings
	alias2entity = gv.dataset.wikidata5m.alias2entity
	alias2relation = gv.dataset.wikidata5m.alias2relation
	found = 0
	not_found = 0
	author2embedding = {}
	for author in tqdm(author2qid,total=len(author2qid.keys())):
	    qid = author2qid[author]
	    if not qid == "NILL":
	        if qid in entity2id:
	            found= found+ 1
	            author2embedding[author] = qid
	        else:
	            not_found = not_found + 1
	author2kg[kg] = author2embedding            
	print(f'Embedded {found} authors not embedded {not_found}')
	 
pickle.dump(author2kg,open(os.path.join(config.pickles,"author2kg.pkl"),"wb"))
